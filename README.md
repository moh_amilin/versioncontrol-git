## Dokumentasi belajar dari Lecture 6: Version Control (git) (2020)

[Link_Video](https://www.youtube.com/watch?v=2sjqTHE0zok&t=1s)

1. mkdir <folder>
2. cd <folder>
3. $ git init
4. $ ls -a
5. $ ls .git
6. $ git status 
    - akan muncul : 
    On branch master
    No commits yet
    nothing added to commit but untracked files present (use "git add" to track)
    - artinya belum ada commit dan riwayat terhadap folder 
7. $ git help init :  untuk membuka dokumentasi git
8. $ echo README.md 
    - echo ; perintah membuat sebuah file
9. $ git status 
    - akan muncul ;
    On branch master
    No commits yet
    Untracked files:
    (use "git add <file>..." to include in what will be committed)
	    README.md       //berwarna merah
    nothing added to commit but untracked files present (use "git add" to track)
10. $ git add README.md
11. $ git status
    - akan muncul ;
    On branch master
    No commits yet
    Changes to be committed:
    (use "git rm --cached <file>..." to unstage)
	    new file:   README.md       // berwarna hijau
12. Dibutuhkan sebuah commit sebagai pemberitahuan apa saja perubahan yang telah dilakukan
    - $ git commit
        - akan muncul sebuah layar baru, tuliskan commit yang anda inginkan
        - kemudian ctrl + x -> ketik y -> ENTER
    - atau bisa dengan cara :
    - $ git commit -m "your commit"
    - Setelah kita ENTER akan muncul hash
        - contoh di laptop saya sepert ini :
        [master (root-commit) ab00a8f] Add README.md as dokumentasi
        - ab00a8f adalah hash yang dihasilkan, 
        - ini fungsinya untuk menjalakan $ git cat-file -p <hash>
13. $ git log
    - akan memunculkan riwayat commit
    - riwayat yang muncul ;
        Author: mohamilin <amilinmoh99@gmail.com>
        Date:   Tue Jan 5 11:56:56 2021 +0700
        
        Add README.md as dokumentasi
14. Jalankan $ git cat-file -p ab00a8f
    - akan muncul ;
    tree 339a40cdc7c320e0521a3a70ea84acbc6e4e6272
    author mohamilin <amilinmoh99@gmail.com> 1609822616 +0700
    committer mohamilin <amilinmoh99@gmail.com> 1609822616 +0700

15. Jalankan lagi $ git cat-file -p 339a40cdc7c320e0521a3a70ea84acbc6e4e6272
    - akan muncul ;
        100644 blob 81b7beee596a3ba20eba27ffd043dce1ffd063f0	README.md
    
    ###### Catatan : 
    - hash bentuk : ab00a8f => memunculkan tree dan hash yang lebih panjang 
                            => memunculkan author dan yang memberikan commit
    - hash bentuk : 339a40cdc7c320e0521a3a70ea84acbc6e4e6272 
                        => memunculkan hash lagi disertai dengan nama file

16. $ git log --all --graph --decorate
    - Akan menampilkan seluruh riwayat commit yang terjadi
        * commit 3d4e3bd685a1f7a2ed4ff08f0fa40f26c4c53adf (HEAD -> master, origin/master)
        | Author: Moh Amilin <amilinmoh99@gmail.com>
        | Date:   Tue Jan 5 13:26:46 2021 +0700
        | 
        |     Update Readme to number 15
        | 
        * commit ab00a8f134d0c5342c27979d71f3672bf2bc47b9
        Author: mohamilin <amilinmoh99@gmail.com>
        Date:   Tue Jan 5 11:56:56 2021 +0700
        
            Add README.md as dokumentasi
    - akan menunjukkan posisi file kita masuk ke dalam branch apa
17. Ketika kita melakukan perubahan baik itu menambah/mengedit/menghapus suatu file/folder <br/>. Kemudian kita lakukan suatu commit maka riwayat dapat dideteksi dengan cara : <br/>
$ git log --all --graph --decorate
18. Menambahkan kata/kalimat dalam sebuah file : $ echo "words" >> filename
19. Melihat/membaca content dari sebuah file : $ cat filename
20. $ git diff filename : untuk melihat riwayat perubahan berdasarkan file
     ###### Catatan : 
     - Untuk poiny 18-19 sertakan type dr filenya (md, text, js, dll)
21. $ git branch namebranch : membuat branch baru
22. $ git checkout namebranch : switch branch yang diinginkan
<!-- stop to 1:00 -->






